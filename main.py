def get_vowels(String):
    return [each for each in String if each in "aeiou"]

def uppercase(String):
    return String.title()